import axios from 'axios';
import {useState, useEffect} from "react";

const apiRoute = 'http://demo1030918.mockable.io/';

export const useFieldAPI = () => {
   const [data, setData] = useState({});
   const [url, setUrl] = useState(apiRoute);
   const [isLoading, setIsLoading] = useState(false);
   const [isError, setIsError] = useState(false);

   useEffect(() => {
      const fetchData = async () => {
         setIsError(false);
         setIsLoading(true);

         try {
            const result = await axios(url);

            setData(result.data);
         } catch (error) {
            setIsError(true);
         }

         setIsLoading(false);
      };

      fetchData();
   }, [url]);

   return [{data, isLoading, isError}, setUrl];
};