import React, {useState} from 'react';
import { makeStyles } from '@material-ui/core/styles';
import {useFieldAPI} from '../api/FieldAPI';
import {useEffect} from 'react';

const useStyles = makeStyles(theme => ({
    container: {
        width: '100%',
    },
    controls: {
        display: 'flex',
        alignItems: 'center',
        marginBottom: 30,
    },
    formControl: {
        marginRight: '20px',
        '& select': {
            padding: 5,
            borderRadius: '5px'
        }
    },
    startBtn: {
        textTransform: 'uppercase',
        fontWeight: 600,
        padding: '7px 20px',
        border: 'none',
        outline: 'none',
        backgroundColor: 'blue',
        color: 'white',
        borderRadius: '5px',
        transition: 'all .25s',
        cursor: 'pointer',
        '&:hover': {
            opacity: .8,
        }
    },
    gameField: {
        display: 'grid',
        gridTemplateRows: props => `${'1fr '.repeat(props.count)}`,
        gridGap: '2px',
        width: '500px',
        maxWidth: '100%',
        height: '500px',
        maxHeight: 'calc(100vw - 40px)',
        background: 'black',
        border: '2px solid black',
    },
    row: {
        display: 'grid',
        gridTemplateColumns: props => `${'1fr '.repeat(props.count)}`,
        gridGap: '2px',
    },
    item: {
        background: 'white',
        transition: 'all .25s',
        '&:hover': {
            background: 'blue',
        }
    }

}));

const Game = ({reset, setMouseEnter}) => {
    const [{ data, isLoading, isError }] = useFieldAPI();
    const [mode, setMode] = useState('');
    const [modes, setModes] = useState([]);
    const [started, setStarted] = useState(false);
    const [field, setField] = useState([]);
    const classes = useStyles({count: (data && mode) ? data[mode].field : 0});

    useEffect(() => {
        const modes = [];
        Object.keys(data).forEach(key => modes.push(key));
        setModes(modes);
    }, [data]);

    useEffect(() => {
        if (mode) {
            const field = new Array(data[mode].field);
            for (let i = 0; i < field.length; i++) {
                const subArr = new Array(data[mode].field);
                for (let j = 0; j < subArr.length; j++) {
                    subArr[j] = {};
                }
                field[i] = subArr
            }
            setField(field);
        }
    }, [mode, data]);

    const handleModeSelect = (event) => {
        const selectedMode = event.target.value;
        setMode(selectedMode);
        setStarted(false);
        reset();
    };

    const handleStart = () => {
        setStarted(true);
    };

    if (isLoading) {
        return <div>Loading...</div>
    }

    if (isError) {
        return <div>Can`t load data, sorry...</div>
    }

    return (
        <div className={classes.container}>
            <div className={classes.controls}>
                {modes && (
                    <div className={classes.formControl}>
                        <select
                            name="select-mode"
                            value={mode || ''}
                            onChange={handleModeSelect}
                        >
                            {!mode && <option value="">Pick mode</option>}
                            {modes.map((mode,index) => (
                                <option key={index} value={mode}>{mode}</option>
                            ))}
                        </select>
                    </div>
                )}
                {mode && !started && (
                    <button className={classes.startBtn} onClick={handleStart}>Start</button>
                )}
            </div>
            {started && (
                <div className={classes.gameField}>
                    {field.map((row, rowIndex) => (
                        <div className={classes.row} key={rowIndex + new Date()}>
                            {row.map((column, columnIndex) => (
                                <div
                                    className={classes.item}
                                    key={columnIndex + new Date()}
                                    onMouseEnter={() => setMouseEnter(rowIndex + 1, columnIndex + 1)}
                                />
                            ))}
                        </div>
                    ))}
                </div>
            )}
        </div>
    );
};

export default Game;
