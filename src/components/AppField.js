import React, {useState} from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Game from "./Game";
import Story from "./Story";

const useStyles = makeStyles(theme => ({
    container: {
        width: '100%',
        maxWidth: '1200px',
        padding: '20px',
        margin: '0 auto',
        display: 'flex',
        justifyContent: 'space-between',
        alignItems: 'flex-start',
        [theme.breakpoints.down(768)]: {
            flexDirection: 'column',
            alignItems: 'center',
        }
    },
    gameWrap: {
        width: '63%',
        maxWidth: '500px',
        [theme.breakpoints.down(768)]: {
            width: '100%',
            marginBottom: '20px',
        }
    },
    historyWrap: {
        width: '30%',
        [theme.breakpoints.down(768)]: {
            width: '100%',
            maxWidth: '500px',
        }
    },
}));

const AppField = () => {
    const classes = useStyles();
    const [history, setHistory] = useState([]);

    const handleMouseEnter = (row, column) => {
        const tempHistory = [...history];
        tempHistory.unshift({row, column});
        setHistory(tempHistory);
    };

    const handleReset = () => {
        setHistory([])
    };

    return (
        <div className={classes.container}>
            <div className={classes.gameWrap}>
                <Game reset={() => handleReset()} setMouseEnter={(row, column) => handleMouseEnter(row, column)}/>
            </div>
            <div className={classes.historyWrap}>
                {history.length > 0 && <Story story={history}/>}
            </div>
        </div>
    );
};

export default AppField;
