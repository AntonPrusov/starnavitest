import React from 'react';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles(theme => ({
    container: {
        width: '100%',
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'flex-start',
    },
    heading: {
        fontSize: '30px',
        fontWeight: 'bold',
        marginBottom: '20px'
    },
    lastItems: {
        maxHeight: '500px',
        overflow: 'auto',
        paddingRight: '20px',
        width: '100%',
    },
    storyItem: {
        background: 'yellow',
        border: '1px solid blue',
        borderRadius: '5px',
        marginBottom: '5px',
        padding: '5px',
    }
}));

const Story = ({story}) => {
    const classes = useStyles();
    return (
        <div className={classes.container}>
            <p className={classes.heading}>History</p>
            <div className={classes.lastItems}>
                {story.map((item, index) => (
                    <p className={classes.storyItem} key={index + new Date()}>
                        Row: {item.row}, column: {item.column}
                    </p>
                ))}
            </div>
        </div>
    );
};

export default Story;
